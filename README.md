

###  Project introduction

This is an example project, containing a simple function with bugs. [Fuzz testing](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/) usually is very efficient to detect bugs like this.

### Understanding the bug
This code consist of a library, and a command line program.

The code is a dummy code (just for the sake of the example)
that triggers a heap out-of-bound access when it is passed the correct
"secret" flag.

```text
int test_string(const char *src, size_t len)
{
    if (len < strlen(FLAG)) {
        return 0;
    } else {
        if (strncmp(src, FLAG, strlen(FLAG)) == 0) {
            // TRIGGER HEAP OVERFLOW READ
            if (src[len] == 0) {
                return -2;
            }
            return -1;
        } else {
            return 0;
        }
    }

}
```

when the secret flag is passed, the code accesses an off-by-one in the `src`
array.

### Libfuzzer introduction

Fuzzing for C/C++ can help find both various complex and critical security. C/C++ are both memory unsafe languages were
memory corruption bugs can lead to serious security vulnerabilities.

### Building and running a libfuzzer target

```yml

  script:
    - apt-get update -qq && apt-get install -y -qq git make clang cmake
    - export CC=`which clang`
    - export CXX=`which clang++`
    - mkdir -p build && cd build
    - cmake .. -DCMAKE_BUILD_TYPE=AddressSanitizer && make && cd ..
    - ./gitlab-cov-fuzz run --regression=$REGRESSION -- ./build/fuzz/fuzz_test_string
    
```

